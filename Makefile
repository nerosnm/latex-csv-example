LATEX=pdflatex
LATEXOPT=--shell-escape --interaction=nonstopmode

LATEXMK=latexmk
LATEXMKOPT=--pdf -f

DOCUMENT=latex-csv-example

RMPDFFILES=ls -a . | grep '$(DOCUMENT)' | egrep -v '\.tex' | xargs rm -rf
RMLATEXFILES=ls -a . | grep '$(DOCUMENT)' | egrep -v '(pdf|\.tex)' | xargs rm -rf
RMAUXFILES=rm -rf **/*.aux

default: document

document: $(DOCUMENT).tex
	$(LATEXMK) $(LATEXMKOPT) --jobname=$(DOCUMENT) -pdflatex="$(LATEX) $(LATEXOPT) %O %S" $(DOCUMENT).tex
	$(RMLATEXFILES)
	$(RMAUXFILES)

clean:
	$(RMLATEXFILES)
	$(RMAUXFILES)

git-prepare: document clean
